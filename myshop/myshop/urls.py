"""myshop URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.urls import path, include
from django.contrib import admin
from django.conf import settings
from django.conf.urls.static import static
from . import views

from catalog import views as viewcatalog

handler404 = 'myshop.views.error_404'
handler500 = 'myshop.views.error_500'
handler403 = 'myshop.views.error_403'
handler400 = 'myshop.views.error_400'

urlpatterns = [
    # URL a la lista de productos
    path('', viewcatalog.product_list, name="product_list"),
    # URL a la app del catálogo
    path('shop/', include('catalog.urls')),
    # URL a la app de administración de Django
    path('admin/', admin.site.urls),
    # URL a la app del carrito.
    path('cart/', include('cart.urls')),
    # URL a la app de las órdenes
    path('orders/', include('orders.urls')),
]+ static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
