#!/usr/bin/env python
# -*- coding: utf-8 -*-
#-------------------------------------------------------------------------
# Archivo: urls.py
#
# Descripción:
#   En este archivo se definen las urls de la app del carrito de compras.
#
#   Cada url debe tener la siguiente estructura:
#
#   path( url, vista, nombre_url )
#
#-------------------------------------------------------------------------

from django.urls import path
from . import views

urlpatterns = [
    # URL al detalle del carrito
    path('', views.cart_detail, name='cart_detail'),
    # URL para agregar un producto al carrito
    path('add/<int:product_id>', views.cart_add, name='cart_add'),
    # URL para remover un producto del carrito
    path('remove/(<int:product_id>', views.cart_remove, name='cart_remove'),
]
